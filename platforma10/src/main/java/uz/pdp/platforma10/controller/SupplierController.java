package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.Supplier;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.repository.SupplierRepo;
import uz.pdp.platforma10.service.SupplierService;

import java.util.List;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    @Autowired
    SupplierRepo supplierRepo;


    @GetMapping("/all")
    public List<Supplier> get() {
        return supplierRepo.findAll();
    }


    @PostMapping("/add")
    public ApiResponse add(@RequestBody Supplier supplier) {
        return supplierService.add(supplier);
    }


    @PutMapping("/update/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody Supplier supplier) {
        return supplierService.update(id, supplier);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return supplierService.delete(id);
    }

}
