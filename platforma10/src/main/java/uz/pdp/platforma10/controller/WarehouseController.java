package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.Currency;
import uz.pdp.platforma10.entity.Warehouse;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.repository.CurrencyRepo;
import uz.pdp.platforma10.repository.WarehouseRepo;
import uz.pdp.platforma10.service.CurrencyService;
import uz.pdp.platforma10.service.WarehouseService;

import java.util.List;

@RestController
@RequestMapping("/warehouse")
public class WarehouseController {

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    WarehouseRepo warehouseRepo;


    @GetMapping("/all")
    public List<Warehouse> get() {
        return warehouseRepo.findAll();
    }


    @PostMapping("/addOrEdit")
    public ApiResponse addOrEdit(@RequestBody Warehouse warehouse) {
        return warehouseService.addOrEdit(warehouse);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return warehouseService.delete(id);
    }

}
