package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.Product;
import uz.pdp.platforma10.entity.User;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.payload.ProductDto;
import uz.pdp.platforma10.repository.ProductRepo;
import uz.pdp.platforma10.service.ProductService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")
public class ProductController {


    @Autowired
    ProductService productService;

    @Autowired
    ProductRepo productRepo;

    @GetMapping()
    public List<Product> get() {
        return productRepo.findAll();
    }

    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return productService.getById(id);
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addProduct(@RequestBody ProductDto dto) {
        return productService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return productService.delete(id);
    }


}
