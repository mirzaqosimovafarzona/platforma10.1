package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.InputProduct;
import uz.pdp.platforma10.entity.OutputProduct;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.payload.InputProductDto;
import uz.pdp.platforma10.payload.OutputDto;
import uz.pdp.platforma10.payload.OutputProductDto;
import uz.pdp.platforma10.repository.InputProductRepo;
import uz.pdp.platforma10.repository.OutputProductRepo;
import uz.pdp.platforma10.service.InputProductService;
import uz.pdp.platforma10.service.OutputProductService;

import java.util.List;

@RestController
@RequestMapping("/outputProduct")
public class OutputProductController {

    @Autowired
    OutputProductService outputProductService;

    @Autowired
    OutputProductRepo outputProductRepo;


    @GetMapping
    public List<OutputProduct> get() {
        return outputProductRepo.findAll();
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addOrUpdate(@RequestBody OutputProductDto dto) {
        return outputProductService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return outputProductService.delete(id);
    }


}
