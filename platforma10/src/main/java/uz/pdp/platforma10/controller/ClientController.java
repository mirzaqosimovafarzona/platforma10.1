package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.Client;
import uz.pdp.platforma10.entity.Measurement;
import uz.pdp.platforma10.entity.Supplier;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.repository.ClientRepo;
import uz.pdp.platforma10.repository.MeasurementRepo;
import uz.pdp.platforma10.service.ClientService;
import uz.pdp.platforma10.service.MeasurementService;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    ClientService clientService;

    @Autowired
    ClientRepo clientRepo;


    @GetMapping("/all")
    public List<Client> get() {
        return clientRepo.findAll();
    }


    @PostMapping("/add")
    public ApiResponse add(@RequestBody Client client) {
        return clientService.add(client);
    }


    @PutMapping("/update/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody Client client) {
        return clientService.update(id, client);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return clientService.delete(id);
    }

}
