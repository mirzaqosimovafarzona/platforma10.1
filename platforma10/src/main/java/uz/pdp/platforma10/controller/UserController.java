package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.User;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.payload.UserDto;
import uz.pdp.platforma10.repository.UserRepo;
import uz.pdp.platforma10.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    UserRepo userRepo;

    @Autowired
    UserService userService;

    // Get All Users
    @GetMapping("/all")
    public List<User> getAll() {
        return userRepo.findAll();
    }


    // Get by User id
    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return userService.getById(id);
    }


    @PostMapping("/save")
    public ApiResponse save(@RequestBody UserDto dto) {
        return userService.save(dto);
    }


    @PutMapping("/update/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody UserDto dto) {
        return userService.update(id, dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return userService.delete(id);
    }
}
