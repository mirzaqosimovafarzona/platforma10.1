package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.Currency;
import uz.pdp.platforma10.entity.Measurement;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.repository.CurrencyRepo;
import uz.pdp.platforma10.repository.MeasurementRepo;
import uz.pdp.platforma10.service.CurrencyService;
import uz.pdp.platforma10.service.MeasurementService;

import java.util.List;

@RestController
@RequestMapping("/currency")
public class CurrencyController {

    @Autowired
    CurrencyService currencyService;

    @Autowired
    CurrencyRepo currencyRepo;


    @GetMapping("/all")
    public List<Currency> get() {
        return currencyRepo.findAll();
    }


    @PostMapping("/addOrEdit")
    public ApiResponse addOrEdit(@RequestBody Currency currency) {
        return currencyService.addOrEdit(currency);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return currencyService.delete(id);
    }

}
