package uz.pdp.platforma10.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.platforma10.entity.Output;
import uz.pdp.platforma10.payload.ApiResponse;
import uz.pdp.platforma10.payload.OutputDto;
import uz.pdp.platforma10.repository.OutputRepo;
import uz.pdp.platforma10.service.OutputService;

import java.util.List;

@RestController
@RequestMapping("/output")
public class OutputController {


    @Autowired
    OutputService outputService;

    @Autowired
    OutputRepo outputRepo;

    @GetMapping()
    public List<Output> get() {
        return outputRepo.findAll();
    }

    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return outputService.getById(id);
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addOrUpdate(@RequestBody OutputDto dto) {
        return outputService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return outputService.delete(id);
    }


}
