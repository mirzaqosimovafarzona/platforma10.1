package uz.pdp.platforma10.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.InputProduct;

public interface InputProductRepo extends JpaRepository<InputProduct, Long> {
}
