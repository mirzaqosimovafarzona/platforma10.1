package uz.pdp.platforma10.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Category;

public interface CategoryRepo extends JpaRepository<Category, Long> {


}
