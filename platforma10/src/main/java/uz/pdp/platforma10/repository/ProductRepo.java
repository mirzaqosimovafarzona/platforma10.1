package uz.pdp.platforma10.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {


    boolean existsByNameAndCategoryId(String name, Long category_id);

}
