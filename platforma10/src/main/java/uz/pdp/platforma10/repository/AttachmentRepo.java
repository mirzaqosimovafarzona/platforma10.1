package uz.pdp.platforma10.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.platforma10.entity.attachment.Attachment;

public interface AttachmentRepo extends JpaRepository<Attachment, Long> {
}
