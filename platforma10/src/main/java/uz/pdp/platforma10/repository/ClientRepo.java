package uz.pdp.platforma10.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Client;

public interface ClientRepo extends JpaRepository<Client, Long> {

    boolean existsByPhoneNumber(String phoneNumber);

}
