package uz.pdp.platforma10.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Input;
import uz.pdp.warehouseprojectlesson11.entity.Output;

public interface OutputRepo extends JpaRepository<Output, Long> {

}
