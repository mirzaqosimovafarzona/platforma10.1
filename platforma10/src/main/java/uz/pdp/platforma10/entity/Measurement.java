package uz.pdp.platforma10.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.platforma10.entity.temlate.AbsNameIdActive;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Measurement extends AbsNameIdActive {


}
